version="7.0.0"

sudo yum install httpd curl mlocate haveged wget
sudo yum install java-1.8.0-openjdk-devel
yum install httpd mod_auth_mellon mod_ssl openssl


sudo sh -c "echo '127.0.0.1 heroes.org' >> /etc/hosts"
sudo systemctl start httpd

# return to hombe git workspace
cd ..

sudo mkdir  /var/www/heroes.org
sudo mkdir /var/www/heroes.org/public_html
sudo cp index.html /var/www/heroes.org/public_html
sudo cp heroes.org.conf /etc/httpd/conf.d/heroes.org.conf

sudo updatedb
sudo systemctl restart httpd

sudo mkdir -p /etc/httpd/saml2
#sudo cd /etc/httpd/saml2
## mellon SP metadata generation 
  

sudo /usr/libexec/mod_auth_mellon/mellon_create_metadata.sh http://heroes.org:8888 http://heroes.org:8888/mellon



sudo cp http_heroes.org_8888.key /etc/httpd/saml2
sudo cp http_heroes.org_8888.cert /etc/httpd/saml2
sudo cp http_heroes.org_8888.xml  /etc/httpd/saml2
#sudo cp meta_idp.xml /etc/httpd/saml2
